There are Five Test suites and each suite contains several test cases.
Details are, 
Test Suite_01: Verify_Add_Product_in_Cart
	Test Case_01: Verify_Add_Product_in_Cart (Go into product details)
	Test Case_02: Verify_Add_Product_in_Cart (not Go into product details)

Test Suite_02: Verify_Add_Product_in_WishList
	Test Case_01: Verify_Add_Product_in_WishList (Non-register user)
	Test Case_02: Verify_Add_Product_in_WishList (Register user)

Test Suite_03: Verify_Login_Process
	Test Case_01: Verify_Login_Process (With Blank Email and Password)
	Test Case_02: Verify_Login_Process (With Invalid Email and Valid Password)
	Test Case_03: Verify_Login_Process (With Valid Email and Invalid Password)
	Test Case_04: Verify_Login_Process (With Invalid Email and Password)
	Test Case_05: Verify_Login_Process (With Valid Email and Password)

Test Suite_04: Verify_Product_Search
	Test Case_01: Verify_Product_Search (as Non-Register User)
	Test Case_01: Verify_Product_Search (as Registered User)

Test_Suite_05: Verify_Registration_Process
	Test Case_01: Verify_Registration_Process (with Blank Email and Password)
	Test Case_02: Verify_Registration_Process (with New Email and Password)
	Test Case_03: Verify_Registration_Process (with Duplicate Email and Password)





